package com.riyug.assignments.test;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.riyug.assignments.main.TwoDArrayTraversalSpiralWay;

class SpiralTraversalTest {

	private List<Integer> expectedresult = new ArrayList<>();
	
	@BeforeEach
	public void initializeList() {
		expectedresult = List.of(1,2,3,4,8,12,16,15,14,13,9,5,6,7,11,10);
	}
	
	@Test
	public void testSpiralOrder() {
		TwoDArrayTraversalSpiralWay TwoDArrayTraversalSpiralWay = new TwoDArrayTraversalSpiralWay();
		int[][] inputArray = {{1,2,3,4},
				{5,6,7,8},
				{9,10,11,12},
				{13,14,15,16}};
		
		List<Integer> solution = TwoDArrayTraversalSpiralWay.traverseInSpiralWay(inputArray);
		assertArrayEquals(expectedresult.toArray(), solution.toArray());
	}

}
